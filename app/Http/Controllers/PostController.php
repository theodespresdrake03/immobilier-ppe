<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Logements;
use App\Models\Personne;
use App\Models\Personnes;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('immobilier');
    }
    public function show($id){
        $post = Logements::findorfail($id);
        $adresse = Adresse::findorfail($id);
        $personne = Personnes::findorfail($id);
        return view('article', [
            'post' => $post,
            'adresse' => $adresse,
            'personne' => $personne
        ]);
    }
    public function create(){
        return view('form');
    }

    public function store(Request $request)
    {
        Logements::create($request->all());
        //dd($this->hasmany('App\Models\Adresse', 'adresse_id'));
        return view('immobilier');
        //return $this->hasmany('App\Models\Adresse', 'adresse_id');
    }
    public function storeadresse(Request $request)
    {
        Adresse::create($request->all());
        dd('Adresse créer');
    }
    public function storepersonne(Request $request)
    {
        Personnes::create($request->all());
        dd('Personne créer');
    }

    public function immobilier(){
        return view('immobilier');

    }

    public function change(Request $request, Personnes $personne, Logements $logements, Adresse $adresse)
    {
        //$request->validate([
        //    "prenom" => "required",
        //    "nom" => "required",
        //    "proprietaire" => "required",
        //    "locataire" => "required",
        //    "ville" => "required",
        //    "rue" => "required",
        //    "code_postal" => "required",
        //    "numero" => "required",
        //    "complement" => "required",
        //    "nombrepiece" => "required",
        //    "surface" => "required",
        //    "prix" => "required",
        //    "disponibilite" => "required",
        //    "date_dispo" => "required",
        //    "style" => "required",
        //]);
        //dd($personne->prenom);
        $personne->update([
            "prenom" => $request->prenom,
            "nom" => $request->nom,
            "proprietaire" => $request->proprietaire,
            "locataire" => $request->locataire,
        ]);
        //dd($personne->prenom);
        $logements->update([
            "nombrepiece" => $request->nombrepiece,
            "surface" => $request->surface,
            "prix" => $request->prix,
            "dispoonibilite" => $request->disponibilite,
            "date_dispo" => $request->date_dispo,
            "style" => $request->style,
        ]);
        //dd($logements->nombrepiece);
        $adresse->update([
            "ville" => $request->ville,
            "rue" => $request->rue,
            "code_postal" => $request->code_postal,
            "numero" => $request->numero,
            "complement" => $request->complement,
        ]);
    }
    public function logement(){
        $logements = Logements::all();
        return view('logement', compact('logements'));
    }
    public function adresse(){
        $adresses = Adresse::all();
        return view('adresse', compact('adresses'));
    }
    public function personne(){
        $personnes = Personnes::all();
        return view('personne', compact('personnes'));
    }
    public function modiflogement($id){
        $post = Logements::findorfail($id);
        return view('modiflogement', [
            'post' => $post,
        ]);
    }
    public function modifadresse($id){
        $post = Adresse::findorfail($id);
        return view('modifadresse', [
            'post' => $post,
        ]);
    }
    public function modifpersonne($id){
        $post = Personnes::findorfail($id);
        return view('modifpersonne', [
            'post' => $post,
        ]);
    }
    public function deletelogement($id)
    {
        Logements::destroy($id);
        return view('supprimer');
    }
    public function deletepersonne($id)
    {
        Personnes::destroy($id);
        return view('supprimer');
    }
    public function deleteadresse($id)
    {
        Adresse::destroy($id);
        return view('supprimer');
    }
    public function createlogement(){
        return view('createlogement');
    }
    public function createadresse(){
        return view('createadresse');
    }
    public function createpersonne(){
        return view('createpersonne');
    }
    public function assologement(){

        return view('assologement');
    }
    public function assoadresse(){

        return view('assoadresse');
    }
    public function assopersonne(){

        return view('assopersonne');
    }
}

