<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Logements;
use App\Models\Personnes;
use Illuminate\Http\Request;

class PersonneController extends Controller
{
    public function index()
    {
        return view('immobilier');
    }
    public function show($id){
        $personne = Personnes::findorfail($id);
        return view('article', [
            'personne' => $personne
        ]);
    }
    public function create(){
        return view('form');
    }
    public function storepersonne(Request $request)
    {
        Personnes::create($request->all());
        return view('immobilier');
    }

    public function immobilier(){
        return view('immobilier');

    }

    public function change(Request $request, Personnes $personne)
    {
        $personne->update([
            "prenom" => $request->prenom,
            "nom" => $request->nom,
            "proprietaire" => $request->proprietaire,
            "locataire" => $request->locataire,
        ]);
        return view('immobilier');
    }
    public function personne(){
        $personnes = Personnes::all();
        return view('personne', compact('personnes'));
    }
    public function modifpersonne($id){
        $post = Personnes::findorfail($id);
        return view('modifpersonne', [
            'post' => $post,
        ]);
    }
    public function deletepersonne($id)
    {
        Personnes::destroy($id);
        Adresse::destroy($id);
        return view('supprimer');
    }
    public function createpersonne(){
        return view('createpersonne');
    }
    public function assopersonne($id){
        $personnes = Personnes::find($id);
        $foo = NULL;                                                            // valeur = true
        $toto = (is_null($personnes->adresse));                                 // si ya association = false sinon true
        if ($toto == $foo) {                                                    //si true = true renvoyer la page sinon autre page
            return view('assopersonne', compact('personnes'));
        }else{
            return view('assosolopersonne', compact('personnes'));
        }
    }
}
