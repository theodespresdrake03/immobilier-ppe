<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Logements;
use App\Models\Personnes;
use Illuminate\Http\Request;

class LogementController extends Controller
{
    public function index()
    {
        return view('immobilier');
    }
    public function show($id){
        $post = Logements::findorfail($id);
        return view('article', [
            'post' => $post
        ]);
    }
    public function create(){
        return view('form');
    }

    public function store(Request $request)
    {
        Logements::create($request->all());
        dd('Logement créer');
    }
    public function immobilier(){
        return view('immobilier');

    }

    public function change(Request $request)
    {
        $logement = Logements::all();
        //dd($logement);
        $logement->update([
            "nombrepiece" => $request->nombrepiece,
            "surface" => $request->surface,
            "prix" => $request->prix,
            "disponibilite" => $request->disponibilite,
            "date_dispo" => $request->date_dispo,
            "style" => $request->style,
        ]);
        return view('immobilier');

    }
    public function logement(){
        $logements = Logements::all();
        return view('logement', compact('logements'));
    }
    public function modiflogement($id){
        $post = Logements::findorfail($id);
        return view('modiflogement', [
            'post' => $post,
        ]);
    }
    public function deletelogement($id)
    {
        Logements::destroy($id);
        Adresse::destroy($id);
        return view('supprimer');
    }
    public function createlogement(){
        return view('createlogement');
    }

    public function assologement($id){
        $logements = Logements::find($id);
        $foo = NULL;                                                            // valeur = true
        $toto = (is_null($logements->adresse));                                 // si ya association = false sinon true
        if ($toto == $foo) {                                                    //si true = true renvoyer la page sinon autre page
            return view('assologement', compact('logements'));
        }else{
            return view('assosolologement', compact('logements'));
        }
    }
}
