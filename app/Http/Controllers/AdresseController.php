<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Logements;
use App\Models\Personnes;
use Illuminate\Http\Request;

class AdresseController extends Controller
{
    public function index()
    {
        return view('immobilier');
    }
    public function show($id){
        $adresse = Adresse::findorfail($id);
        return view('article', [
            'adresse' => $adresse,
        ]);
    }
    public function create(){
        return view('form');
    }

    public function storeadresse(Request $request)
    {
        Adresse::create($request->all());
        return view('immobilier');
    }

    public function immobilier(){
        return view('immobilier');

    }

    public function change(Request $request)
    {
        $adresse = Adresse::all();
        //dd($adresse);
        $adresse->update([
            "ville" => $request->ville,
            "rue" => $request->rue,
            "code_postal" => $request->code_postal,
            "numero" => $request->numero,
            "complement" => $request->complement,
        ]);
        return view('immobilier');
    }
    public function adresse(){
        $adresses = Adresse::all();
        return view('adresse', compact('adresses'));
    }
    public function modifadresse($id){
        $post = Adresse::findorfail($id);
        return view('modifadresse', [
            'post' => $post,
        ]);
    }
    public function deleteadresse($id)
    {
        Adresse::destroy($id);
        Personnes::destroy($id);
        Logements::destroy($id);
        return view('supprimer');
    }
    public function createadresse(){
        return view('createadresse');
    }
    public function assoadresse($id){
        $adresses = Adresse::find($id);
        //dd($adresses->logements);
        $foo = NULL;                                                            // valeur = true
        $toto = (is_null($adresses->logement));                                 // si ya association = false sinon true
        if ($toto == $foo) {                                                    //si true = true renvoyer la page sinon autre page
            return view('assoadresse', compact('adresses'));
        }else{
            return view('assosoloadresse', compact('adresses'));
        }
    }
}
