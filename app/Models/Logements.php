<?php

namespace App\Models;
use App\Models\Adresse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logements extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['nombrepiece','surface','prix','disponibilite','date_dispo','style'];
    public function adresse()
    {
        return $this->BelongsTo(\App\Models\Adresse::class);
    }
    use HasFactory;
}
