<?php

namespace App\Models;

use App\Models\adresse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personnes extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['prenom','nom','proprietaire','locataire'];
    public function adresses()
    {
        return $this->BelongsTo(\App\Models\Adresse::class);
    }
    use HasFactory;
}
