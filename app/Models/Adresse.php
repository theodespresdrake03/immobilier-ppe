<?php

namespace App\Models;
use App\Models\personnes;
use App\Models\logements;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adresse extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['ville','rue','code_postal','numero','complement'];
    public function logements(){
        return $this->hasMany(logements::class);
    }
    public function personnes(){
        return $this->hasMany(personnes::class);
    }


    use HasFactory;
}
