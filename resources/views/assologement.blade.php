@extends('layouts.app')

@section('content')

    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Nombre de pièces</th>
                <th>Surface en m2</th>
                <th>Prix</th>
                <th>Disponibilité (1=oui 0=non)</th>
                <th>Date Dispo</th>
                <th>Style de la maison</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$logements->nombrepiece}}</td>
                    <td>{{$logements->surface}}</td>
                    <td>{{$logements->prix}}</td>
                    <td>{{$logements->disponibilite}}</td>
                    <td>{{$logements->date_dispo}}</td>
                    <td>{{$logements->style}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Ville</th>
                <th>Rue</th>
                <th>Code_Postal</th>
                <th>Numéro</th>
                <th>Complement</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$logements->adresse->ville}}</td>
                <td>{{$logements->adresse->rue}}</td>
                <td>{{$logements->adresse->code_postal}}</td>
                <td>{{$logements->adresse->numero}}</td>
                <td>{{$logements->adresse->complement}}</td>
            </tr>
            </tbody>
        </table>
    </div>


@endsection
