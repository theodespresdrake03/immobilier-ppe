@extends('layouts.app')

@section('content')

    <style>
    body {
    color:white;
    background-image : url('{{ asset('images/imageun.jpeg') }}');
    background-size: cover;
    background-position:  center center;
    background-repeat: no-repeat;
    }
    </style>

    <center>
        <h1>Id de la maison: {{$post->id}}
    <h1> Nombre de pièce: {{$post->nombrepiece}}<input type="text" name="nombrepiece" value="{{$post->nombrepiece}}" class="border-gray-600 border-2"></h1>
    <h1> Surface en M2: {{$post->surface}}<input type="text" name="surface" value="{{$post->surface}}" class="border-gray-600 border-2"></h1>
    <h1> Prix: {{$post->prix}}<input type="text" name="prix" value="{{$post->prix}}" class="border-gray-600 border-2"></h1>
    <h1> Disponibilité (1=oui 0=non): {{$post->disponibilite}}<input type="text" name="disponibilite" value="{{$post->disponibilite}}" class="border-gray-600 border-2"></h1>
    <h1> Date Dispo: {{$post->date_dispo}}<input type="text" name="date_dispo" value="{{$post->date_dispo}}" class="border-gray-600 border-2"></h1>
    <h1> Style de la maison: {{$post->style}}<input type="text" name="style" value="{{$post->style}}" class="border-gray-600 border-2"></h1>
    <h1> Ville: {{$adresse->ville}}<input type="text" name="ville" value="{{$adresse->ville}}" class="border-gray-600 border-2"></h1>
    <h1> Adresse: {{$adresse->rue}}<input type="text" name="rue" value="{{$adresse->rue}}" class="border-gray-600 border-2"></h1>
    <h1> Code Postal: {{$adresse->code_postal}}<input type="text" name="code_postal" value="{{$adresse->code_postal}}" class="border-gray-600 border-2"></h1>
    <h1> Numéro: {{$adresse->numero}}<input type="text" name="numero" value="{{$adresse->numero}}" class="border-gray-600 border-2"></h1>
    <h1> Complement (0 si rien): {{$adresse->complement}}<input type="text" name="complement" value="{{$adresse->complement}}" class="border-gray-600 border-2"></h1>
    <h1> Prenom: {{$personne->prenom}}<input type="text" name="prenom" value="{{$personne->prenom}}" class="border-gray-600 border-2"></h1>
    <h1> Nom: {{$personne->nom}}<input type="text" name="nom" value="{{$personne->nom}}" class="border-gray-600 border-2"></h1>
    <h1> Proprietaire (1=oui 0=non): {{$personne->proprietaire}}<input type="text" name="proprietaire" value="{{$personne->proprietaire}}" class="border-gray-600 border-2"></h1>
            <h1> Locataire (1=oui 0=non): {{$personne->locataire}}<input type="text" name="locataire" value="{{$personne->locataire}}" class="border-gray-600 border-2"></h1></h1>


    </center>
@endsection

