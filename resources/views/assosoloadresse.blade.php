@extends('layouts.app')

@section('content')
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Ville</th>
                <th>Rue</th>
                <th>Code Postal</th>
                <th>Numéro</th>
                <th>Complement</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$adresses->ville}}</td>
                <td>{{$adresses->rue}}</td>
                <td>{{$adresses->code_postal}}</td>
                <td>{{$adresses->numero}}</td>
                <td>{{$adresses->complement}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
