@extends('layouts.app')

@section('content')
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Ville</th>
                <th>Rue</th>
                <th>Code Postal</th>
                <th>Numéro</th>
                <th>Complement</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$adresses->ville}}</td>
                <td>{{$adresses->rue}}</td>
                <td>{{$adresses->code_postal}}</td>
                <td>{{$adresses->numero}}</td>
                <td>{{$adresses->complement}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Nombre de pièce</th>
                <th>Surface en M2</th>
                <th>Prix</th>
                <th>Disponibilite</th>
                <th>Date de disponibilite</th>
                <th>Style</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$adresses->logement->nombrepiece}}</td>
                <td>{{$adresses->logement->surface}}</td>
                <td>{{$adressses->logement->disponibilite}}</td>
                <td>{{$adresses->logement->date_dispo}}</td>
                <td>{{$adresses->logement->style}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Prenom</th>
                <th>Nom</th>
                <th>Proprietaire</th>
                <th>Locataire</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$adresses->personne->prenom}}</td>
                <td>{{$adresses->personne->nom}}</td>
                <td>{{$adressses->personne->proprietaire}}</td>
                <td>{{$adresses->personne->locataire}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
