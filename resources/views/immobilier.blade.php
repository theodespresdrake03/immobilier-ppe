<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon site d'immobilier</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">
</head>
<body>
<header>
    <nav>
        <ul>
            <li><a href="{{ route('layout') }}">Accueil</a></li>
            <li><a href="{{ route('logement') }}">Logements</a></li>
            <li><a href="{{ route('adresse') }}">Adresses</a></li>
            <li><a href="{{ route('personne') }}">Personnes</a></li>
        </ul>
    </nav>
</header>
</body>
<style>
    *{
        box-sizing: border-box;
        color: #181818;
        font-family: 'Abril Fatface', cursive;
        font-size: 20px;
    }
    body{
        padding: 0;
        margin: 0;
    }
    header {
        color:white;
        background-image : url('{{ asset('images/back.png') }}');
        background-size: cover;
        background-position:  center center;
        background-repeat: no-repeat;
        height: 100vh;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    ul{
        margin: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 30px;
        padding: 30px;
    }
    li{
        list-style: none;
        padding:10px  20px;
        border: 3px solid #181818;
        transition: box-shadow ease-in-out 0.2s;
    }
    li:hover{
        box-shadow: 0 0 13px  #858585;
    }
    a{
        text-decoration: none;
    }
</style>
</html>


