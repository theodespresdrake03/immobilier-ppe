@extends('layouts.app')

@section('content')
    <div class="fade">
        <a class="button right" href="{{ route('createpersonne')}}">crée une personne</a>
        <table>
            <thead>
            <tr>
                <th>Prenom</th>
                <th>Nom</th>
                <th>Proprietaire</th>
                <th>Locataire</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($personnes as $personne)
                <tr>
                    <td>{{$personne->prenom}}</td>
                    <td>{{$personne->nom}}</td>
                    <td> {{$personne->proprietaire}}</td>
                    <td>{{$personne->locataire}}</td>
                    <td>
                        <a href="{{ route('modifpersonne', [$personne->id])}}">Modifier une personne</a>
                        <a href="{{ route('assopersonne', [$personne->id])}}">Voir l'adresse associé</a>
                        <form method="POST" action="{{ route('deletepersonne', [$personne->id])}}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Supprimer la Personne"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
