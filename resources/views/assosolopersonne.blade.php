@extends('layouts.app')

@section('content')
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Prenom</th>
                <th>Nom</th>
                <th>Proprietaire</th>
                <th>Locataire</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$personnes->prenom}}</td>
                <td>{{$personnes->nom}}</td>
                <td>{{$personnes->proprietaire}}</td>
                <td>{{$personnes->locataire}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
