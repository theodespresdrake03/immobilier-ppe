<nav>
    <ul>
        <li><a href="{{ route('layout') }}">Accueil</a></li>
        <li><a href="{{ route('logement') }}">Logement</a></li>
        <li><a href="{{ route('adresse') }}">Adresse</a></li>
        <li><a href="{{ route('personne') }}">Personne</a></li>
    </ul>
</nav>
<style>
    ul{
        background-color: #C5C5C5;
    }
    body{
        margin: 0;
    }
    body {
        background-image : url('{{ asset('images/back.png') }}');
        background-size: cover;
        background-position:  center center;
        background-repeat: no-repeat;
        min-height: 100vh;
    }
    nav{
        font-family: 'Abril Fatface', cursive;
        font-size: 20px;
    }
    nav ul{
        margin: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 30px;
        padding: 30px;
    }
    nav li{
        list-style: none;
    }
    nav li a {
        color: #181818;
        text-decoration: none;
    }
    .fade{
        margin: 30px;
        background-color: rgba(255,255,255,0.8);
        padding: 30px;
    }
    table{
        width: 100%;
        margin-top: 30px;
    }
    table > *{
        text-align: center;
    }
    table thead{
        font-family: 'Abril Fatface', cursive;
        font-size: 15px;
        font-weight: 100;
    }
    table tr th{
        padding: 20px 0;
        margin: 0;
    }
    table tr td{
        margin: 0;
        padding: 20px 0;
        border-top: 2px solid #181818;
    }
    table{
        border-spacing: 0;
    }
    .button{
        padding:10px  20px;
        border: 3px solid #181818;
        transition: box-shadow ease-in-out 0.2s;
        color: #181818;
        text-decoration: none;
        font-family: 'Abril Fatface', cursive;
        font-size: 15px;
    }
    .button:hover{
        box-shadow: 0 0 13px  #858585;
    }
    .right{
        float: right;
    }
    table a{
        text-decoration: none;
        color: #181818;
        display: block;
    }
    .form, form{
        box-sizing: border-box;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        flex-direction: column;

    }
    .form > *, form >*{
        font-family: 'Abril Fatface', cursive;
        font-size: 15px;
    }
    .form input, form input{
        margin: 10px 30px 20px 30px;
        padding:10px  20px;
        border: 3px solid #181818;
        transition: box-shadow ease-in-out 0.2s;
        color: #181818;
        text-decoration: none;
        font-family: inherit;
        font-size: 15px;
        width: 90%;
        text-align: center;
    }
    h1{
        text-align: center;
    }
</style>
