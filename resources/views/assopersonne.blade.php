@extends('layouts.app')

@section('content')
    @extends('layouts.app')

@section('content')
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Prenom</th>
                <th>Nom</th>
                <th>Proprietaire</th>
                <th>Locataire</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$personnes->prenom}}</td>
                <td>{{$personnes->nom}}</td>
                <td>{{$personnes->proprietaire}}</td>
                <td>{{$personnes->locataire}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Ville</th>
                <th>Rue</th>
                <th>Code Postal</th>
                <th>Numéro</th>
                <th>Complement</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$personnes->adresse->ville}}</td>
                <td>{{$personnes->adresse->rue}}</td>
                <td>{{$personnes->adresse->code_postal}}</td>
                <td>{{$personnes->adresse->numero}}</td>
                <td>{{$personne->adresse->complement}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
