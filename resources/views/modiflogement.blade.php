@extends('layouts.app')

@section('content')
    <div class="fade">
        <form method="GET" action="{{ route('changelogement', [$post->id])}}">
            @csrf
        <div class="form">
            <h1> Nombre de pièce: {{$post->nombrepiece}}</h1>
            <input type="text" name="nombrepiece" value="{{$post->nombrepiece}}" class="border-gray-600 border-2">
            <h1> Surface en M2: {{$post->surface}}</h1>
            <input type="text" name="surface" value="{{$post->surface}}" class="border-gray-600 border-2">
            <h1> Prix: {{$post->prix}}</h1>
            <input type="text" name="prix" value="{{$post->prix}}" class="border-gray-600 border-2">
            <h1> Disponibilité (1=oui 0=non): {{$post->disponibilite}}</h1>
            <input type="text" name="disponibilite" value="{{$post->disponibilite}}" class="border-gray-600 border-2">
            <h1> Date Dispo: {{$post->date_dispo}}</h1>
            <input type="text" name="date_dispo" value="{{$post->date_dispo}}" class="border-gray-600 border-2">
            <h1> Style de la maison: {{$post->style}}</h1>
            <input type="text" name="style" value="{{$post->style}}" class="border-gray-600 border-2">
        </div>
            <input type="submit" value="Modifier le logement"/>
        </form>

    </div>
@endsection
