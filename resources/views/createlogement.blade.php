@extends('layouts.app')
@section('content')
    <div class="fade">
        <h1>Nouveau Logement</h1>
        <form method="POST" action="{{ route('posts.store') }}">
            @csrf
            <input type="text" name="nombrepiece" placeholder="Nombre de pièce" class="border-gray-600 border-2">
            <input type="text" name="surface" placeholder="Surface en M2" class="border-gray-600 border-2">
            <input type="text" name="prix" placeholder="Prix" class="border-gray-600 border-2">
            <input type="text" name="disponibilite" placeholder="Disponible (1=oui 0=non)" class="border-gray-600 border-2">
            <input type="text" name="date_dispo" placeholder="Date Dispo" class="border-gray-600 border-2">
            <input type="text" name="style" placeholder="Style" class="border-gray-600 border-2">

            <button type="submit">créer</button>
        </form>
    </div>
@endsection
