@extends('layouts.app')

@section('content')

    <div class="fade">
        <table>
            <thead>
            <tr>
                <th>Nombre de pièces</th>
                <th>Surface en m2</th>
                <th>Prix</th>
                <th>Disponibilité (1=oui 0=non)</th>
                <th>Date Dispo</th>
                <th>Style de la maison</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$logements->nombrepiece}}</td>
                <td>{{$logements->surface}}</td>
                <td>{{$logements->prix}}</td>
                <td>{{$logements->disponibilite}}</td>
                <td>{{$logements->date_dispo}}</td>
                <td>{{$logements->style}}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
