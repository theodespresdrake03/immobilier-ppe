@extends('layouts.app')

@section('content')
    <style>
    </style>
    <div class="fade">
        <a class="button right" href="{{ route('createlogement')}}">crée un logement</a>
        <table>
            <thead>
            <tr>
                <th>Nombre de pièces</th>
                <th>Surface en m2</th>
                <th>Prix</th>
                <th>Disponibilité (1=oui 0=non)</th>
                <th>Date Dispo</th>
                <th>Style de la maison</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($logements as $logement)
            <tr>
                <td>{{$logement->nombrepiece}}</td>
                <td>{{$logement->surface}}</td>
                <td>{{$logement->prix}}</td>
                <td>{{$logement->disponibilite}}</td>
                <td>{{$logement->date_dispo}}</td>
                <td>{{$logement->style}}</td>
                <td>
                    <a href="{{ route('modiflogement', [$logement->id])}}">Modifier le Logement</a>
                    <a href="{{ route('assologement', [$logement->id])}}">Voir l'adresse associée</a>
                    <form method="POST" action="{{ route('deletelogement', [$logement->id])}}">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Supprimer le logement"/>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
