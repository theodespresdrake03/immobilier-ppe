@extends('layouts.app')

@section('content')

    <div class="fade">
        <a class="button right" href="{{ route('createadresse')}}">crée une adresse</a>
        <table>
            <thead>
            <tr>
                <th>Ville</th>
                <th>Rue</th>
                <th>Code Postal</th>
                <th>Numero</th>
                <th>Complement</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($adresses as $adresse)
                <tr>
                    <td>{{$adresse->ville}}</td>
                    <td>{{$adresse->rue}}</td>
                    <td> {{$adresse->code_postal}}</td>
                    <td>{{$adresse->numero}}</td>
                    <td>{{$adresse->complement}}</td>
                    <td>
                        <a href="{{ route('modifadresse', [$adresse->id])}}">Modifier l'adresse</a>
                        <a href="{{ route('assoadresse', [$adresse->id])}}">Voir le logement et la personne associé</a>
                        <form method="POST" action="{{ route('deleteadresse', [$adresse->id])}}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Supprimer l'adresse"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
