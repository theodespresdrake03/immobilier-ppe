@extends('layouts.app')
@section('content')
    <div class="fade">
        <h1>Nouvelle Personne</h1>
        <form method="POST" action="{{ route('posts.storepersonne') }}">
            @csrf
            <input type="text" name="prenom" placeholder="Prenom" class="border-gray-600 border-2">
            <input type="text" name="nom" placeholder="Nom" class="border-gray-600 border-2">
            <input type="text" name="proprietaire" placeholder="Proprietaire (1=oui 0=non)" class="border-gray-600 border-2">
            <input type="text" name="locataire" placeholder="Locataire (1=oui 0=non)" class="border-gray-600 border-2">
            <button type="submit">créer</button>
        </form>
    </div>
@endsection
