@extends('layouts.app')
@section('content')
    <div class="fade">
        <h1>Nouvelle Adresse</h1>
        <form method="POST" action="{{ route('posts.storeadresse') }}">
            @csrf
            <input type="text" name="ville" placeholder="Ville" class="border-gray-600 border-2">
            <input type="text" name="rue" placeholder="Rue" class="border-gray-600 border-2">
            <input type="text" name="code_postal" placeholder="Code Postal" class="border-gray-600 border-2">
            <input type="text" name="numero" placeholder="Numero" class="border-gray-600 border-2">
            <input type="text" name="complement" placeholder="Complement" class="border-gray-600 border-2">
            <button type="submit">créer</button>
        </form>
    </div>
@endsection
