@extends('layouts.app')

@section('content')
    <div class="fade">
        <form method="GET" action="{{ route('changepersonne', [$post->id])}}">
            @csrf
        <div class="form">
            <h1>Id de la personne: {{$post->id}}</h1>
            <h1> Prenom: {{$post->prenom}}</h1>
            <input type="text" name="prenom" value="{{$post->prenom}}" class="border-gray-600 border-2">
            <h1> Nom: {{$post->nom}}</h1>
            <input type="text" name="nom" value="{{$post->nom}}" class="border-gray-600 border-2">
            <h1> Proprietaire: {{$post->proprietaire}}</h1>
            <input type="text" name="proprietaire" value="{{$post->proprietaire}}" class="border-gray-600 border-2">
            <h1> Locataire: {{$post->locataire}}</h1>
            <input type="text" name="locataire" value="{{$post->locataire}}" class="border-gray-600 border-2">
            <h1> Adresse associé: {{$post->adresse_id}}</h1>
            <input type="text" name="adresse_id" value="{{$post->adresse_id}}" class="border-gray-600 border-2">
        </div>
            <input type="submit" value="Modifier la personne"/>
        </form>
    </div>
@endsection
