<?php

namespace Tests\Feature;

use App\Logements;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_vue_immo()
    {
        $response = $this->get('/immobilier');

        $response->assertStatus(200);
    }
    public function test_vue_create()
    {
        $response = $this->get('/createlogement');

        $response->assertStatus(200);
    }
    public function test_vue_logement()
    {
        $response = $this->get('/logement');

        $response->assertStatus(200);
    }
}
