<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index'])->name('layout');

Route::get('/posts/create', [PostController::class, 'create'])->name('posts.create');
Route::post('/immobilierstore', [PostController::class, 'store'])->name('posts.store');
Route::post('/adressestore', [\App\Http\Controllers\AdresseController::class, 'storeadresse'])->name('posts.storeadresse');
Route::post('/personnestore', [\App\Http\Controllers\PersonneController::class, 'storepersonne'])->name('posts.storepersonne');
Route::delete('/deletepersonne/{id}', [\App\Http\Controllers\PersonneController::class, 'deletepersonne'])->name('deletepersonne');
Route::delete('/deletelogement/{id}', [\App\Http\Controllers\LogementController::class, 'deletelogement'])->name('deletelogement');
Route::delete('/deleteadresse/{id}', [\App\Http\Controllers\AdresseController::class, 'deleteadresse'])->name('deleteadresse');
Route::get('/change/{personne}', [\App\Http\Controllers\PersonneController::class, 'change'])->name('changepersonne');
Route::get('/change', [\App\Http\Controllers\LogementController::class, 'change'])->name('changelogement');
Route::get('/change', [\App\Http\Controllers\AdresseController::class, 'change'])->name('changeadresse');
Route::get('/posts/{id}', [PostController::class, 'show'])->name('posts.show');
Route::get('/immobilier', [PostController::class, 'immobilier'])->name('immobilier');

Route::get('/logement', [\App\Http\Controllers\LogementController::class, 'logement'])->name('logement');
Route::get('/adresse', [\App\Http\Controllers\AdresseController::class, 'adresse'])->name('adresse');
Route::get('/personne', [\App\Http\Controllers\PersonneController::class, 'personne'])->name('personne');

Route::get('/modiflogement/{id}', [\App\Http\Controllers\LogementController::class, 'modiflogement'])->name('modiflogement');
Route::get('/modifadresse/{id}', [\App\Http\Controllers\AdresseController::class, 'modifadresse'])->name('modifadresse');
Route::get('/modifpersonne/{id}', [\App\Http\Controllers\PersonneController::class, 'modifpersonne'])->name('modifpersonne');

Route::get('/createlogement', [\App\Http\Controllers\LogementController::class, 'createlogement'])->name('createlogement');
Route::get('/createadresse', [\App\Http\Controllers\AdresseController::class, 'createadresse'])->name('createadresse');
Route::get('/createpersonne', [\App\Http\Controllers\PersonneController::class, 'createpersonne'])->name('createpersonne');

Route::get('/assologement/{id}', [\App\Http\Controllers\LogementController::class, 'assologement'])->name('assologement');
Route::get('/assoadresse/{id}', [\App\Http\Controllers\AdresseController::class, 'assoadresse'])->name('assoadresse');
Route::get('/assopersonne/{id}', [\App\Http\Controllers\PersonneController::class, 'assopersonne'])->name('assopersonne');

Route::put('/modiflogement/{id}', [\App\Http\Controllers\LogementController::class, 'change'])->name('boutonmodiflogement');
Route::put('/modifadresse/{id}', [\App\Http\Controllers\AdresseController::class, 'change'])->name('boutonmodifadresse');
Route::put('/modifpersonne/{id}', [\App\Http\Controllers\PersonneController::class, 'change'])->name('boutonmodifpersonne');
