<?php

namespace Database\Factories;

use App\Models\Adresse;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdresseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Adresse::class;
    public function definition()
    {
        return [
            'ville' => $this->faker->country,
            'rue' => $this->faker->streetAddress(),
            'code_postal' => $this->faker->numberBetween(10000,99999),
            'numero' => $this->faker->numberBetween(1, 999),
            'complement' => $this->faker->text()
        ];
    }
}
