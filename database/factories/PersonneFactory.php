<?php

namespace Database\Factories;

use App\Models\Personnes;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Personnes::class;
    public function definition()
    {
        return [
            'prenom' => $this->faker->firstName(),
            'nom' => $this->faker->lastName(),
            'proprietaire' => $this->faker->boolean,
            'locataire' => $this->faker->boolean,
        ];
    }
}
