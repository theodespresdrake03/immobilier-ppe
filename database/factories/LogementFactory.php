<?php
namespace Database\Factories;

use App\Models\Logements;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Logements::class;

    public function definition()
    {
        return [
            'nombrepiece' => $this->faker->numberBetween(1,500),
            'surface' => $this->faker->numberBetween(1,500),
            'prix' => $this->faker->numberBetween(1,500),
            'disponibilite' => $this->faker->boolean,
            'date_dispo' => $this->faker->date,
            'style' => $this->faker->text
        ];
    }
}
