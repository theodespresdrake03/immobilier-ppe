<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logements', function (Blueprint $table) {
            $table->id();
            $table->integer('nombrepiece');
            $table->integer('surface');
            $table->integer('prix');
            $table->boolean('disponibilite');
            $table->date('date_dispo');
            $table->text('style');
            $table->timestamps();

            $table->unsignedBigInteger('adresse_id')->nullable();
            $table->foreign('adresse_id')->references('id')->on('adresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logements');
    }
}
