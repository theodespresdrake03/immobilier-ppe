<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



class CreateAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adresses', function (Blueprint $table) {
            $table->id();

            $table->text('ville');
            $table->text('rue');
            $table->integer('code_postal');
            $table->integer('numero');
            $table->text('complement');
            $table->timestamps();

            $table->unsignedBigInteger('logement_id')->nullable();
            $table->foreign('logement_id')->references('id')->on('logements');
            $table->unsignedBigInteger('personne_id')->nullable();
            $table->foreign('personne_id')->references('id')->on('personnes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adresses');
        }
}
